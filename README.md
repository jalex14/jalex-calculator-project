Calculator Project

## Description
A basic calculator. For now it adds, subtracts, and divides. If you want to try it, download the code and run it in an IDE.

## Roadmap
I want to add exponentation, square rooting, and the ability to resize the window in the future.

## Authors and acknowledgment
Joseph A.
